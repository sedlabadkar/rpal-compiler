/*
 * parser.h
 *
 *  Created on: Mar 1, 2016
 *      Author: sachin
 */

#ifndef PARSER_H_
#define PARSER_H_
#include "lexer.h"
#include <stack>

using namespace std;

class treeNode{
public:
	string nodeString;
	treeNode* childNode = NULL;
	treeNode* siblingNode = NULL;
};

class parser {
public:

	parser (lexer*);
	void printAST();
	virtual ~parser();

private:
    lexer* lex;
    stack <treeNode*> treeStack;
    token *nextToken;

	void E();
	void Ew();
	void T();
	void Ta();
	void Tc();
	void B();
	void Bt();
	void Bs();
	void Bp();
	void A();
	void At();
	void Af();
	void Ap();
	void R();
	void Rn();
	void D();
	void Da();
	void Dr();
	void Db();
	void Vb();
	void Vl();

	void parse();
    void buildTree(string, int); //Tree node name & child nodes num
    void treePrettyPrint(treeNode*, int);
    void read(string);
	bool isKeyword(string);

};

#endif /* PARSER_H_ */
